# -*- coding: utf-8 -*-
from setuptools import setup

with open("README.rst") as readme_file:
    readme = readme_file.read()

with open("HISTORY.rst") as history_file:
    history = history_file.read()

requirements = ["click>=6.0", "csentry-api>=0.4.0"]

test_requirements = ["pytest", "pytest-cov", "responses"]

setup(
    name="csentry-inventory",
    author="Benjamin Bertrand",
    author_email="benjamin.bertrand@esss.se",
    description="CSEntry Dynamic Inventory script for Ansible",
    long_description=readme + "\n\n" + history,
    url="https://gitlab.esss.lu.se/ics-infrastructure/csentry-inventory",
    license="MIT license",
    version="1.0.0",
    install_requires=requirements,
    test_suite="tests",
    tests_require=test_requirements,
    py_modules=["csentry_inventory"],
    include_package_data=True,
    keywords="csentry",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 2",
        "Programming Language :: Python :: 2.7",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
    ],
    entry_points={"console_scripts": ["csentry-inventory=csentry_inventory:cli"]},
)
