#!/usr/bin/env python
# Copyright (c) 2018 European Spallation Source ERIC
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
import click
import json
from csentry import CSEntry


def to_json(data, pretty_print=False):
    """Convert data to a JSON string"""
    if pretty_print:
        return json.dumps(data, sort_keys=True, indent=4)
    else:
        return json.dumps(data)


class CSEntryInventory(object):
    def __init__(self, url, token):
        self._inventory = {"_meta": {"hostvars": {}}, "all": {"hosts": []}}
        self._networks = {}
        self._hosts_to_include = set()
        self.csentry = CSEntry(url=url, token=token)

    def get_host_vars(self, host):
        """Return all variables associated to host"""
        vars = host["ansible_vars"] or {}
        try:
            vars["csentry_user"] = host["user"]
        except TypeError:
            click.echo(
                "Warning! Ignoring host {} Ansible vars (not a dict): {}\n".format(
                    host["name"], vars
                ),
                err=True,
            )
            vars = {"csentry_user": host["user"]}
        vars["csentry_device_type"] = host["device_type"]
        vars["csentry_is_ioc"] = host["is_ioc"]
        vars["csentry_host_id"] = host["id"]
        vars["csentry_interfaces"] = []
        for interface in host["interfaces"]:
            csentry_interface = {
                key: value
                for key, value in interface.items()
                if key in ("name", "is_main", "ip", "netmask", "cnames", "tags", "mac")
            }
            network_name = interface["network"]
            csentry_interface["gateway"] = self._networks[network_name]["gateway"]
            csentry_interface["network"] = {
                "name": network_name,
                "vlan_id": self._networks[network_name]["vlan_id"],
                "domain": self._networks[network_name]["domain"],
            }
            if interface["is_main"]:
                vars["ansible_host"] = interface["ip"]
            vars["csentry_interfaces"].append(csentry_interface)
        if not host["interfaces"]:
            click.echo(
                "Warning! No interface found for {}\n".format(host["name"]), err=True
            )
        vars["csentry_model"] = host["model"]
        vars["csentry_items"] = host["items"]
        return vars

    def retrieve_networks(self):
        # Get all networks as a dict of vlan_name: network
        networks = self.csentry.get_networks(per_page=100)
        self._networks = {network["vlan_name"]: network for network in networks}

    def retrieve_csentry_groups(self, filter_groups):
        # Get the list of all groups from CSEntry
        groups = self.csentry.get_groups(per_page=100)
        # Convert this list to a dict with the group names as key
        all_groups = {}
        for group in groups:
            all_groups[group["name"]] = {
                "hosts": sorted(group["hosts"]),
                "vars": group["vars"] or {},
                "children": sorted(group.get("children", [])),
            }
        if filter_groups:
            # Save all the hosts part of the groups to filter in
            # the self._hosts_to_include set
            def update_hosts_to_include(grp):
                # Recursively add hosts from the current group and all children
                self._hosts_to_include.update(set(grp["hosts"]))
                for child_group_name in grp["children"]:
                    try:
                        child_group = all_groups[child_group_name]
                    except KeyError:
                        pass
                    else:
                        update_hosts_to_include(child_group)

            for name, group in all_groups.items():
                if name in filter_groups:
                    update_hosts_to_include(group)
            # Only keep those hosts in the different groups
            for name, group in all_groups.items():
                # No need to filter on the filter_groups
                # (we want all hosts part of those groups)
                if name not in filter_groups:
                    group["hosts"] = [
                        host
                        for host in group["hosts"]
                        if host in self._hosts_to_include
                    ]
        # Update the inventory with all groups
        self._inventory.update(all_groups)

    def inventory(self, filter_groups=None):
        """Return the inventory as a dict"""
        if filter_groups is None:
            filter_groups = []
        self.retrieve_csentry_groups(filter_groups)
        groups = [key for key in self._inventory.keys() if key not in ("all", "_meta")]
        self.retrieve_networks()
        hostvars = {
            host["fqdn"]: self.get_host_vars(host)
            for host in self.csentry.get_hosts(recursive=True, per_page=100)
            if (not filter_groups) or (host["fqdn"] in self._hosts_to_include)
        }
        all_hosts = list(hostvars.keys())
        self._inventory["_meta"]["hostvars"] = hostvars
        self._inventory["all"]["hosts"] = sorted(all_hosts)
        self._inventory["all"]["children"] = sorted(groups)
        return self._inventory

    def host_vars(self, hostname):
        """Return the host variables as a dict"""
        host = self.csentry.get_one("/network/hosts", name=hostname, recursive=True)
        self.retrieve_networks()
        return self.get_host_vars(host)


@click.command()
@click.version_option()
@click.option("--list", is_flag=True, help="List all CSEntry hosts")
@click.option("--host", help="Only get variables for a specific host")
@click.option("--group", multiple=True, help="Only return hosts part of this group")
@click.option(
    "--csentry-url",
    help="CSEntry URL. Override CSENTRY_URL environment variable.",
    envvar="CSENTRY_URL",
)
@click.option(
    "--csentry-token",
    help="CSEntry token. Override CSENTRY_TOKEN environment variable.",
    envvar="CSENTRY_TOKEN",
)
@click.option(
    "--pretty", is_flag=True, help="Pretty print JSON output [default: False]"
)
def cli(csentry_url, csentry_token, host, list, group, pretty):
    """Retrieve CSEntry inventory

    If CSENTRY_URL and CSENTRY_TOKEN are not set as environment variables,
    they have to be passed as option.
    """
    if csentry_url is None or not csentry_url.startswith("http"):
        raise click.UsageError(
            "Invalid CSEntry url. If CSENTRY_URL is not set, you have to pass --csentry-url."
        )
    if csentry_token is None:
        raise click.UsageError(
            "If CSENTRY_TOKEN is not set, you have to pass --csentry-token."
        )
    ci = CSEntryInventory(url=csentry_url, token=csentry_token)
    result = {}
    if list:
        result = ci.inventory(filter_groups=group)
    elif host:
        # Make sure to always pass the short name and not FQDN
        result = ci.host_vars(host.split(".")[0])
    click.echo(to_json(result, pretty_print=pretty))


if __name__ == "__main__":
    cli()
