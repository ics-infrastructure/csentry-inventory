=======
History
=======

1.0.0 (2010-10-08)
------------------

* Recursively include all hosts from children groups when filtering (INFRA-2670)

0.9.1 (2010-10-07)
------------------

* Remove check on filtered groups children (INFRA-2670)

0.9.0 (2019-10-04)
------------------

* Add gateway to secondary interfaces (INFRA-1350)

0.8.1 (2019-08-20)
------------------

* Fix inventory sync failure due to invalid Ansible vars (INFRA-1221)

0.8.0 (2019-07-10)
------------------

* Add csentry_model and csentry_items to hostvars (INFRA-1111)

0.7.1 (2019-04-29)
------------------

* Improve performances by increasing per_page when calling the API

0.7.0 (2019-02-08)
------------------

* Add host id to host variables (INFRA-800)

0.6.0 (2019-02-01)
------------------

* Allow to filter per group (INFRA-780)
* Add mac address to hostvars (INFRA-783)

0.5.0 (2019-01-23)
------------------

* Pass user in host variables (INFRA-760)

0.4.0 (2018-11-30)
------------------

* Use FQDN for inventory hostname (INFRA-640)
* Add extra variables to each host (INFRA-640)
* Use pre-commit to run black and flake8
* Add more tests

WARNING! Requires csentry >= 0.15.0

0.3.0 (2018-08-17)
------------------

* Add children groups

0.2.0 (2018-07-16)
------------------

* Remove "ANSIBLE\_" prefix from env variables name

0.1.0 (2018-07-16)
------------------

* Initial release
