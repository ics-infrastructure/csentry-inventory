# -*- coding: utf-8 -*-
import os
import pytest
import responses
from click.testing import CliRunner
from csentry_inventory import cli, CSEntryInventory


@pytest.fixture(scope="module", autouse=True)
def clear_env_vars():
    os.environ.pop("CSENTRY_URL", None)
    os.environ.pop("CSENTRY_TOKEN", None)


def test_no_csentry_url():
    runner = CliRunner()
    result = runner.invoke(cli)
    assert result.exception
    assert "Invalid CSEntry url" in result.output


def test_no_csentry_token():
    runner = CliRunner()
    result = runner.invoke(cli, ["--csentry-url", "http://localhost:8000"])
    assert result.exception
    assert (
        "If CSENTRY_TOKEN is not set, you have to pass --csentry-token" in result.output
    )


def test_cli_options():
    runner = CliRunner()
    result = runner.invoke(
        cli,
        [
            "--csentry-url",
            "https://csentry.example.com",
            "--csentry-token",
            "xxxxxxxxx",
        ],
    )
    assert not result.exception
    assert result.output.strip() == "{}"


def test_env_variables():
    runner = CliRunner()
    result = runner.invoke(
        cli,
        env={
            "CSENTRY_URL": "https://csentry.example.com",
            "CSENTRY_TOKEN": "xxxxxxxxxxxxxxxxxxxxxx",
        },
    )
    assert not result.exception
    assert result.output.strip() == "{}"


@responses.activate
def test_inventory_single_host():
    url = "http://csentry.example.org"
    networks_data = [
        {
            "vlan_name": "mynetwork",
            "vlan_id": 42,
            "domain": "example.org",
            "gateway": "192.168.0.254",
        },
        {
            "vlan_name": "anothernetwork",
            "vlan_id": 1001,
            "domain": "example.org",
            "gateway": "192.168.1.254",
        },
    ]
    hosts_data = [
        {
            "id": 42,
            "name": "myhost",
            "user": "johndoe",
            "fqdn": "myhost.example.org",
            "device_type": "VirtualMachine",
            "is_ioc": False,
            "items": [],
            "model": None,
            "interfaces": [
                {
                    "name": "myhost",
                    "is_main": True,
                    "ip": "192.168.0.10",
                    "netmask": "255.255.255.0",
                    "network": "mynetwork",
                    "mac": "02:42:42:a7:c9:c5",
                    "cnames": [],
                    "tags": [],
                },
                {
                    "name": "myhost-2",
                    "is_main": False,
                    "ip": "192.168.1.10",
                    "netmask": "255.255.255.0",
                    "network": "anothernetwork",
                    "cnames": [],
                    "tags": [],
                },
            ],
            "ansible_vars": {"foo": "hello"},
            "ansible_groups": [],
        }
    ]
    ci = CSEntryInventory(url=url, token="12345")
    responses.add(
        responses.GET,
        url + "/api/v1/network/hosts?name=myhost&recursive=True",
        json=hosts_data,
        headers={"X-Total-Count": "1"},
        status=200,
    )
    responses.add(
        responses.GET,
        url + "/api/v1/network/networks?per_page=100",
        json=networks_data,
        headers={"X-Total-Count": "2"},
        status=200,
    )
    result = ci.host_vars("myhost")
    assert result == {
        "ansible_host": "192.168.0.10",
        "csentry_device_type": "VirtualMachine",
        "csentry_interfaces": [
            {
                "cnames": [],
                "gateway": "192.168.0.254",
                "ip": "192.168.0.10",
                "is_main": True,
                "mac": "02:42:42:a7:c9:c5",
                "name": "myhost",
                "netmask": "255.255.255.0",
                "network": {
                    "domain": "example.org",
                    "name": "mynetwork",
                    "vlan_id": 42,
                },
                "tags": [],
            },
            {
                "cnames": [],
                "gateway": "192.168.1.254",
                "ip": "192.168.1.10",
                "is_main": False,
                "name": "myhost-2",
                "netmask": "255.255.255.0",
                "network": {
                    "domain": "example.org",
                    "name": "anothernetwork",
                    "vlan_id": 1001,
                },
                "tags": [],
            },
        ],
        "csentry_host_id": 42,
        "csentry_is_ioc": False,
        "csentry_user": "johndoe",
        "csentry_model": None,
        "csentry_items": [],
        "foo": "hello",
    }


@responses.activate
def test_inventory_invalid_ansible_vars():
    url = "http://csentry.example.org"
    networks_data = [
        {
            "vlan_name": "mynetwork",
            "vlan_id": 42,
            "domain": "example.org",
            "gateway": "192.168.0.254",
        }
    ]
    hosts_data = [
        {
            "id": 42,
            "name": "myhost",
            "user": "johndoe",
            "fqdn": "myhost.example.org",
            "device_type": "VirtualMachine",
            "is_ioc": False,
            "items": [],
            "model": None,
            "interfaces": [
                {
                    "name": "myhost",
                    "is_main": True,
                    "ip": "192.168.0.10",
                    "netmask": "255.255.255.0",
                    "network": "mynetwork",
                    "mac": "02:42:42:a7:c9:c5",
                    "cnames": [],
                    "tags": [],
                }
            ],
            "ansible_vars": "vm_owner: - foo - bar",
            "ansible_groups": [],
        }
    ]
    ci = CSEntryInventory(url=url, token="12345")
    responses.add(
        responses.GET,
        url + "/api/v1/network/hosts?name=myhost&recursive=True",
        json=hosts_data,
        headers={"X-Total-Count": "1"},
        status=200,
    )
    responses.add(
        responses.GET,
        url + "/api/v1/network/networks?per_page=100",
        json=networks_data,
        headers={"X-Total-Count": "1"},
        status=200,
    )
    result = ci.host_vars("myhost")
    assert result == {
        "ansible_host": "192.168.0.10",
        "csentry_device_type": "VirtualMachine",
        "csentry_interfaces": [
            {
                "cnames": [],
                "gateway": "192.168.0.254",
                "ip": "192.168.0.10",
                "is_main": True,
                "mac": "02:42:42:a7:c9:c5",
                "name": "myhost",
                "netmask": "255.255.255.0",
                "network": {
                    "domain": "example.org",
                    "name": "mynetwork",
                    "vlan_id": 42,
                },
                "tags": [],
            }
        ],
        "csentry_host_id": 42,
        "csentry_is_ioc": False,
        "csentry_user": "johndoe",
        "csentry_model": None,
        "csentry_items": [],
    }


@responses.activate
def test_inventory():
    url = "http://csentry.example.org"
    groups_data = [
        {
            "name": "group1",
            "type": "STATIC",
            "hosts": ["myhost-01.example.org"],
            "children": [],
            "vars": {"hello": "world"},
        }
    ]
    networks_data = [
        {
            "vlan_name": "mynetwork",
            "vlan_id": 42,
            "domain": "example.org",
            "gateway": "192.168.0.254",
        },
        {
            "vlan_name": "labnetwork",
            "vlan_id": 1001,
            "domain": "lab.example.org",
            "gateway": "192.168.1.254",
        },
    ]
    hosts_data = [
        {
            "id": 42,
            "name": "myhost-01",
            "user": "user1",
            "fqdn": "myhost-01.example.org",
            "device_type": "Network",
            "is_ioc": False,
            "items": [
                {
                    "serial_number": "TC3717130078",
                    "ics_id": "AAB052",
                    "stack_member": 0,
                },
                {
                    "serial_number": "TC3717130075",
                    "ics_id": "AAB053",
                    "stack_member": 1,
                },
            ],
            "model": "EX4600-40F-AFO",
            "interfaces": [
                {
                    "name": "myhost-01",
                    "is_main": True,
                    "ip": "192.168.0.10",
                    "mac": "02:42:42:a7:c9:d5",
                    "netmask": "255.255.255.0",
                    "network": "mynetwork",
                    "cnames": ["myhost"],
                    "tags": [],
                }
            ],
            "ansible_vars": None,
            "ansible_groups": ["group1"],
        },
        {
            "id": 25,
            "name": "foo",
            "user": "user2",
            "fqdn": "foo.lab.example.org",
            "device_type": "VirtualMachine",
            "is_ioc": True,
            "items": [],
            "model": None,
            "interfaces": [
                {
                    "name": "foo",
                    "is_main": True,
                    "ip": "192.168.1.32",
                    "mac": "02:42:42:a7:c9:32",
                    "netmask": "255.255.255.0",
                    "network": "labnetwork",
                    "cnames": [],
                    "tags": ["IOC"],
                }
            ],
            "ansible_vars": None,
            "ansible_groups": [],
        },
    ]
    ci = CSEntryInventory(url=url, token="12345")
    responses.add(
        responses.GET,
        url + "/api/v1/network/groups?per_page=100",
        json=groups_data,
        headers={"X-Total-Count": "2"},
        status=200,
    )
    responses.add(
        responses.GET,
        url + "/api/v1/network/hosts?per_page=100&recursive=True",
        json=hosts_data,
        headers={"X-Total-Count": "2"},
        status=200,
    )
    responses.add(
        responses.GET,
        url + "/api/v1/network/networks?per_page=100",
        json=networks_data,
        headers={"X-Total-Count": "2"},
        status=200,
    )
    result = ci.inventory()
    assert result == {
        "_meta": {
            "hostvars": {
                "myhost-01.example.org": {
                    "ansible_host": "192.168.0.10",
                    "csentry_device_type": "Network",
                    "csentry_interfaces": [
                        {
                            "cnames": ["myhost"],
                            "gateway": "192.168.0.254",
                            "ip": "192.168.0.10",
                            "is_main": True,
                            "mac": "02:42:42:a7:c9:d5",
                            "name": "myhost-01",
                            "netmask": "255.255.255.0",
                            "network": {
                                "domain": "example.org",
                                "name": "mynetwork",
                                "vlan_id": 42,
                            },
                            "tags": [],
                        }
                    ],
                    "csentry_host_id": 42,
                    "csentry_is_ioc": False,
                    "csentry_user": "user1",
                    "csentry_items": [
                        {
                            "serial_number": "TC3717130078",
                            "ics_id": "AAB052",
                            "stack_member": 0,
                        },
                        {
                            "serial_number": "TC3717130075",
                            "ics_id": "AAB053",
                            "stack_member": 1,
                        },
                    ],
                    "csentry_model": "EX4600-40F-AFO",
                },
                "foo.lab.example.org": {
                    "ansible_host": "192.168.1.32",
                    "csentry_device_type": "VirtualMachine",
                    "csentry_interfaces": [
                        {
                            "cnames": [],
                            "gateway": "192.168.1.254",
                            "ip": "192.168.1.32",
                            "mac": "02:42:42:a7:c9:32",
                            "is_main": True,
                            "name": "foo",
                            "netmask": "255.255.255.0",
                            "network": {
                                "domain": "lab.example.org",
                                "name": "labnetwork",
                                "vlan_id": 1001,
                            },
                            "tags": ["IOC"],
                        }
                    ],
                    "csentry_host_id": 25,
                    "csentry_is_ioc": True,
                    "csentry_user": "user2",
                    "csentry_model": None,
                    "csentry_items": [],
                },
            }
        },
        "all": {
            "children": ["group1"],
            "hosts": ["foo.lab.example.org", "myhost-01.example.org"],
        },
        "group1": {
            "children": [],
            "hosts": ["myhost-01.example.org"],
            "vars": {"hello": "world"},
        },
    }


@responses.activate
def test_inventory_filter_group():
    url = "http://csentry.example.org"
    groups_data = [
        {
            "name": "group1",
            "type": "DYNAMIC",
            "hosts": ["myhost-01.example.org"],
            "children": [],
            "vars": {"hello": "world"},
        },
        {
            "name": "group2",
            "type": "STATIC",
            "hosts": ["myhost-01.example.org", "myhost-02.lab.example.org"],
            "children": [],
            "vars": {},
        },
    ]
    networks_data = [
        {
            "vlan_name": "mynetwork",
            "vlan_id": 42,
            "domain": "example.org",
            "gateway": "192.168.0.254",
        },
        {
            "vlan_name": "labnetwork",
            "vlan_id": 1001,
            "domain": "lab.example.org",
            "gateway": "192.168.1.254",
        },
    ]
    hosts_data = [
        {
            "id": 42,
            "name": "myhost-01",
            "user": "user1",
            "fqdn": "myhost-01.example.org",
            "device_type": "VirtualMachine",
            "is_ioc": False,
            "items": [],
            "model": None,
            "interfaces": [
                {
                    "name": "myhost-01",
                    "is_main": True,
                    "ip": "192.168.0.10",
                    "netmask": "255.255.255.0",
                    "network": "mynetwork",
                    "cnames": ["myhost"],
                    "tags": [],
                }
            ],
            "ansible_vars": None,
            "ansible_groups": ["group1"],
        },
        {
            "id": 33,
            "name": "myhost-02",
            "user": "user2",
            "fqdn": "myhost-02.lab.example.org",
            "device_type": "VirtualMachine",
            "is_ioc": True,
            "items": [],
            "model": None,
            "interfaces": [
                {
                    "name": "myhost-02",
                    "is_main": True,
                    "ip": "192.168.1.32",
                    "netmask": "255.255.255.0",
                    "network": "labnetwork",
                    "cnames": [],
                    "tags": ["IOC"],
                }
            ],
            "ansible_vars": None,
            "ansible_groups": [],
        },
    ]
    ci = CSEntryInventory(url=url, token="12345")
    responses.add(
        responses.GET,
        url + "/api/v1/network/groups?per_page=100",
        json=groups_data,
        headers={"X-Total-Count": "2"},
        status=200,
    )
    responses.add(
        responses.GET,
        url + "/api/v1/network/hosts?per_page=100&recursive=True",
        json=hosts_data,
        headers={"X-Total-Count": "1"},
        status=200,
    )
    responses.add(
        responses.GET,
        url + "/api/v1/network/networks?per_page=100",
        json=networks_data,
        headers={"X-Total-Count": "2"},
        status=200,
    )
    result = ci.inventory(("group1",))
    assert result == {
        "_meta": {
            "hostvars": {
                "myhost-01.example.org": {
                    "ansible_host": "192.168.0.10",
                    "csentry_device_type": "VirtualMachine",
                    "csentry_interfaces": [
                        {
                            "cnames": ["myhost"],
                            "gateway": "192.168.0.254",
                            "ip": "192.168.0.10",
                            "is_main": True,
                            "name": "myhost-01",
                            "netmask": "255.255.255.0",
                            "network": {
                                "domain": "example.org",
                                "name": "mynetwork",
                                "vlan_id": 42,
                            },
                            "tags": [],
                        }
                    ],
                    "csentry_host_id": 42,
                    "csentry_is_ioc": False,
                    "csentry_user": "user1",
                    "csentry_model": None,
                    "csentry_items": [],
                }
            }
        },
        "all": {"children": ["group1", "group2"], "hosts": ["myhost-01.example.org"]},
        "group1": {
            "children": [],
            "hosts": ["myhost-01.example.org"],
            "vars": {"hello": "world"},
        },
        "group2": {"children": [], "hosts": ["myhost-01.example.org"], "vars": {}},
    }


@responses.activate
def test_inventory_all_group():
    # Test that the "all" group can be defined in CSEntry to define
    # variables
    # Hosts and children should be ignored
    url = "http://csentry.example.org"
    groups_data = [
        {
            "name": "all",
            "type": "STATIC",
            "hosts": [],
            "children": [],
            "vars": {"hello": "world"},
        },
        {
            "name": "group1",
            "type": "STATIC",
            "hosts": ["myhost-01.example.org"],
            "children": [],
            "vars": {},
        },
    ]
    networks_data = [
        {
            "vlan_name": "mynetwork",
            "vlan_id": 42,
            "domain": "example.org",
            "gateway": "192.168.0.254",
        },
        {
            "vlan_name": "labnetwork",
            "vlan_id": 1001,
            "domain": "lab.example.org",
            "gateway": "192.168.1.254",
        },
    ]
    hosts_data = [
        {
            "id": 42,
            "name": "myhost-01",
            "user": "user1",
            "fqdn": "myhost-01.example.org",
            "device_type": "VirtualMachine",
            "is_ioc": False,
            "items": [],
            "model": None,
            "interfaces": [
                {
                    "name": "myhost-01",
                    "is_main": True,
                    "ip": "192.168.0.10",
                    "netmask": "255.255.255.0",
                    "network": "mynetwork",
                    "cnames": ["myhost"],
                    "tags": [],
                }
            ],
            "ansible_vars": None,
            "ansible_groups": ["group1"],
        },
        {
            "id": 33,
            "name": "myhost-02",
            "user": "user2",
            "fqdn": "myhost-02.lab.example.org",
            "device_type": "VirtualMachine",
            "is_ioc": True,
            "items": [],
            "model": None,
            "interfaces": [
                {
                    "name": "myhost-02",
                    "is_main": True,
                    "ip": "192.168.1.32",
                    "netmask": "255.255.255.0",
                    "network": "labnetwork",
                    "cnames": [],
                    "tags": [],
                }
            ],
            "ansible_vars": None,
            "ansible_groups": [],
        },
    ]
    ci = CSEntryInventory(url=url, token="12345")
    responses.add(
        responses.GET,
        url + "/api/v1/network/groups?per_page=100",
        json=groups_data,
        headers={"X-Total-Count": "2"},
        status=200,
    )
    responses.add(
        responses.GET,
        url + "/api/v1/network/hosts?per_page=100&recursive=True",
        json=hosts_data,
        headers={"X-Total-Count": "1"},
        status=200,
    )
    responses.add(
        responses.GET,
        url + "/api/v1/network/networks?per_page=100",
        json=networks_data,
        headers={"X-Total-Count": "2"},
        status=200,
    )
    result = ci.inventory()
    assert result == {
        "_meta": {
            "hostvars": {
                "myhost-01.example.org": {
                    "ansible_host": "192.168.0.10",
                    "csentry_device_type": "VirtualMachine",
                    "csentry_interfaces": [
                        {
                            "cnames": ["myhost"],
                            "gateway": "192.168.0.254",
                            "ip": "192.168.0.10",
                            "is_main": True,
                            "name": "myhost-01",
                            "netmask": "255.255.255.0",
                            "network": {
                                "domain": "example.org",
                                "name": "mynetwork",
                                "vlan_id": 42,
                            },
                            "tags": [],
                        }
                    ],
                    "csentry_host_id": 42,
                    "csentry_is_ioc": False,
                    "csentry_user": "user1",
                    "csentry_model": None,
                    "csentry_items": [],
                },
                "myhost-02.lab.example.org": {
                    "ansible_host": "192.168.1.32",
                    "csentry_device_type": "VirtualMachine",
                    "csentry_interfaces": [
                        {
                            "cnames": [],
                            "gateway": "192.168.1.254",
                            "ip": "192.168.1.32",
                            "is_main": True,
                            "name": "myhost-02",
                            "netmask": "255.255.255.0",
                            "network": {
                                "domain": "lab.example.org",
                                "name": "labnetwork",
                                "vlan_id": 1001,
                            },
                            "tags": [],
                        }
                    ],
                    "csentry_host_id": 33,
                    "csentry_is_ioc": True,
                    "csentry_user": "user2",
                    "csentry_model": None,
                    "csentry_items": [],
                },
            }
        },
        "all": {
            "children": ["group1"],
            "hosts": ["myhost-01.example.org", "myhost-02.lab.example.org"],
            "vars": {"hello": "world"},
        },
        "group1": {"children": [], "hosts": ["myhost-01.example.org"], "vars": {}},
    }


@responses.activate
def test_inventory_filter_group_with_children():
    url = "http://csentry.example.org"
    groups_data = [
        {
            "name": "group1",
            "type": "STATIC",
            "hosts": ["myhost-01.example.org", "myhost-06.example.org"],
            "children": ["group3"],
            "vars": {"hello": "world"},
        },
        {
            "name": "group2",
            "type": "STATIC",
            "hosts": ["myhost-01.example.org", "myhost-02.lab.example.org"],
            "children": [],
            "vars": {},
        },
        {
            "name": "group3",
            "type": "STATIC",
            "hosts": ["myhost-03.example.org", "myhost-06.example.org"],
            "children": ["group4"],
            "vars": {},
        },
        {
            "name": "group4",
            "type": "STATIC",
            "hosts": ["myhost-04.example.org"],
            "children": [],
            "vars": {},
        },
        {
            "name": "group5",
            "type": "STATIC",
            "hosts": ["myhost-05.example.org"],
            "children": [],
            "vars": {},
        },
    ]
    networks_data = [
        {
            "vlan_name": "mynetwork",
            "vlan_id": 42,
            "domain": "example.org",
            "gateway": "192.168.0.254",
        },
        {
            "vlan_name": "labnetwork",
            "vlan_id": 1001,
            "domain": "lab.example.org",
            "gateway": "192.168.1.254",
        },
    ]
    hosts_data = [
        {
            "id": 42,
            "name": "myhost-01",
            "user": "user1",
            "fqdn": "myhost-01.example.org",
            "device_type": "VirtualMachine",
            "is_ioc": False,
            "items": [],
            "model": None,
            "interfaces": [
                {
                    "name": "myhost-01",
                    "is_main": True,
                    "ip": "192.168.0.10",
                    "netmask": "255.255.255.0",
                    "network": "mynetwork",
                    "cnames": ["myhost"],
                    "tags": [],
                }
            ],
            "ansible_vars": None,
            "ansible_groups": ["group1", "group2"],
        },
        {
            "id": 33,
            "name": "myhost-02",
            "user": "user2",
            "fqdn": "myhost-02.lab.example.org",
            "device_type": "VirtualMachine",
            "is_ioc": True,
            "items": [],
            "model": None,
            "interfaces": [
                {
                    "name": "myhost-02",
                    "is_main": True,
                    "ip": "192.168.1.32",
                    "netmask": "255.255.255.0",
                    "network": "labnetwork",
                    "cnames": [],
                    "tags": ["IOC"],
                }
            ],
            "ansible_vars": None,
            "ansible_groups": ["group2"],
        },
        {
            "id": 55,
            "name": "myhost-03",
            "user": "user1",
            "fqdn": "myhost-03.example.org",
            "device_type": "VirtualMachine",
            "is_ioc": False,
            "items": [],
            "model": None,
            "interfaces": [
                {
                    "name": "myhost-03",
                    "is_main": True,
                    "ip": "192.168.0.11",
                    "netmask": "255.255.255.0",
                    "network": "mynetwork",
                    "cnames": [],
                    "tags": [],
                }
            ],
            "ansible_vars": None,
            "ansible_groups": ["group3"],
        },
        {
            "id": 56,
            "name": "myhost-04",
            "user": "user1",
            "fqdn": "myhost-04.example.org",
            "device_type": "VirtualMachine",
            "is_ioc": False,
            "items": [],
            "model": None,
            "interfaces": [
                {
                    "name": "myhost-04",
                    "is_main": True,
                    "ip": "192.168.0.12",
                    "netmask": "255.255.255.0",
                    "network": "mynetwork",
                    "cnames": [],
                    "tags": [],
                }
            ],
            "ansible_vars": None,
            "ansible_groups": ["group4"],
        },
        {
            "id": 57,
            "name": "myhost-05",
            "user": "user1",
            "fqdn": "myhost-05.example.org",
            "device_type": "VirtualMachine",
            "is_ioc": False,
            "items": [],
            "model": None,
            "interfaces": [
                {
                    "name": "myhost-05",
                    "is_main": True,
                    "ip": "192.168.0.13",
                    "netmask": "255.255.255.0",
                    "network": "mynetwork",
                    "cnames": [],
                    "tags": [],
                }
            ],
            "ansible_vars": None,
            "ansible_groups": ["group5"],
        },
        {
            "id": 58,
            "name": "myhost-06",
            "user": "user1",
            "fqdn": "myhost-06.example.org",
            "device_type": "VirtualMachine",
            "is_ioc": False,
            "items": [],
            "model": None,
            "interfaces": [
                {
                    "name": "myhost-06",
                    "is_main": True,
                    "ip": "192.168.0.14",
                    "netmask": "255.255.255.0",
                    "network": "mynetwork",
                    "cnames": [],
                    "tags": [],
                }
            ],
            "ansible_vars": None,
            "ansible_groups": ["group1", "group3"],
        },
    ]
    ci = CSEntryInventory(url=url, token="12345")
    responses.add(
        responses.GET,
        url + "/api/v1/network/groups?per_page=100",
        json=groups_data,
        headers={"X-Total-Count": "5"},
        status=200,
    )
    responses.add(
        responses.GET,
        url + "/api/v1/network/hosts?per_page=100&recursive=True",
        json=hosts_data,
        headers={"X-Total-Count": "5"},
        status=200,
    )
    responses.add(
        responses.GET,
        url + "/api/v1/network/networks?per_page=100",
        json=networks_data,
        headers={"X-Total-Count": "2"},
        status=200,
    )
    result = ci.inventory(("group1",))
    assert result == {
        "_meta": {
            "hostvars": {
                "myhost-01.example.org": {
                    "ansible_host": "192.168.0.10",
                    "csentry_device_type": "VirtualMachine",
                    "csentry_interfaces": [
                        {
                            "cnames": ["myhost"],
                            "gateway": "192.168.0.254",
                            "ip": "192.168.0.10",
                            "is_main": True,
                            "name": "myhost-01",
                            "netmask": "255.255.255.0",
                            "network": {
                                "domain": "example.org",
                                "name": "mynetwork",
                                "vlan_id": 42,
                            },
                            "tags": [],
                        }
                    ],
                    "csentry_host_id": 42,
                    "csentry_is_ioc": False,
                    "csentry_user": "user1",
                    "csentry_model": None,
                    "csentry_items": [],
                },
                "myhost-03.example.org": {
                    "ansible_host": "192.168.0.11",
                    "csentry_device_type": "VirtualMachine",
                    "csentry_interfaces": [
                        {
                            "cnames": [],
                            "gateway": "192.168.0.254",
                            "ip": "192.168.0.11",
                            "is_main": True,
                            "name": "myhost-03",
                            "netmask": "255.255.255.0",
                            "network": {
                                "domain": "example.org",
                                "name": "mynetwork",
                                "vlan_id": 42,
                            },
                            "tags": [],
                        }
                    ],
                    "csentry_host_id": 55,
                    "csentry_is_ioc": False,
                    "csentry_user": "user1",
                    "csentry_model": None,
                    "csentry_items": [],
                },
                "myhost-04.example.org": {
                    "ansible_host": "192.168.0.12",
                    "csentry_device_type": "VirtualMachine",
                    "csentry_interfaces": [
                        {
                            "cnames": [],
                            "gateway": "192.168.0.254",
                            "ip": "192.168.0.12",
                            "is_main": True,
                            "name": "myhost-04",
                            "netmask": "255.255.255.0",
                            "network": {
                                "domain": "example.org",
                                "name": "mynetwork",
                                "vlan_id": 42,
                            },
                            "tags": [],
                        }
                    ],
                    "csentry_host_id": 56,
                    "csentry_is_ioc": False,
                    "csentry_user": "user1",
                    "csentry_model": None,
                    "csentry_items": [],
                },
                "myhost-06.example.org": {
                    "ansible_host": "192.168.0.14",
                    "csentry_device_type": "VirtualMachine",
                    "csentry_interfaces": [
                        {
                            "cnames": [],
                            "gateway": "192.168.0.254",
                            "ip": "192.168.0.14",
                            "is_main": True,
                            "name": "myhost-06",
                            "netmask": "255.255.255.0",
                            "network": {
                                "domain": "example.org",
                                "name": "mynetwork",
                                "vlan_id": 42,
                            },
                            "tags": [],
                        }
                    ],
                    "csentry_host_id": 58,
                    "csentry_is_ioc": False,
                    "csentry_user": "user1",
                    "csentry_model": None,
                    "csentry_items": [],
                },
            }
        },
        "all": {
            "children": ["group1", "group2", "group3", "group4", "group5"],
            "hosts": [
                "myhost-01.example.org",
                "myhost-03.example.org",
                "myhost-04.example.org",
                "myhost-06.example.org",
            ],
        },
        "group1": {
            "children": ["group3"],
            "hosts": ["myhost-01.example.org", "myhost-06.example.org"],
            "vars": {"hello": "world"},
        },
        "group2": {"children": [], "hosts": ["myhost-01.example.org"], "vars": {}},
        "group3": {
            "children": ["group4"],
            "hosts": ["myhost-03.example.org", "myhost-06.example.org"],
            "vars": {},
        },
        "group4": {"children": [], "hosts": ["myhost-04.example.org"], "vars": {}},
        "group5": {"children": [], "hosts": [], "vars": {}},
    }
