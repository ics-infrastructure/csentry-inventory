csentry-inventory
=================

.. image:: https://img.shields.io/badge/code%20style-black-000000.svg
    :target: https://github.com/ambv/black

.. image:: https://gitlab.esss.lu.se/ics-infrastructure/csentry-inventory/badges/master/pipeline.svg

.. image:: https://gitlab.esss.lu.se/ics-infrastructure/csentry-inventory/badges/master/coverage.svg

CSEntry_ Dynamic Inventory script for Ansible_.

Quick start
-----------

This Python package installs an entry-point that can be run directly.
The variables `CSENTRY_URL` and `CSENTRY_TOKEN` shall be set in the environment or can
be passed via the `--csentry-url` and `--csentry-token` command line options.

::

    $ export CSENTRY_URL="https://csentry.esss.lu.se"
    $ export CSENTRY_TOKEN="xxxxxxxxxxxxxxxxxxxxxxxxx"
    $ csentry-inventory --list --pretty

    $ csentry-inventory --help


Installation
------------

::

    $ pip install csentry-inventory -i https://artifactory.esss.lu.se/artifactory/api/pypi/pypi-virtual/simple


License
-------

MIT

.. _CSEntry: http://ics-infrastructure.pages.esss.lu.se/csentry/index.html
.. _Ansible: https://docs.ansible.com/ansible/latest/dev_guide/developing_inventory.html
